package com.nutech.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@PropertySource("classPath:application.properties")
public class AppConfig extends WebMvcConfigurerAdapter 
{
	@Value("${db.driver}")
	private String dbDriver;
	
	@Value("${db.url}")
	private String url;
	
	@Value("${db.userName}")
	private String userName;
	
	@Value("${db.password}")
	private String password;
	
	
	@Bean
	public DriverManagerDataSource ds() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(dbDriver);
		ds.setUrl(url);
		ds.setUsername(userName);
		ds.setPassword(password);
		System.out.println(userName);
		return ds;
}
	@Bean 
	public JdbcTemplate jt()
	{
	JdbcTemplate jt = new JdbcTemplate();
	jt.setDataSource(ds());
	return jt;
	}
}
