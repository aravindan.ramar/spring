package com.nutech.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.nutech.bean.RegisterBean;

@Repository
public interface UserRepository {

	public int create(RegisterBean register);

	public List<RegisterBean> getUsers();

	public int delete(int id);

	public RegisterBean getEmpById(int id);

	public int update(RegisterBean reg);
}
