package com.nutech.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nutech.bean.RegisterBean;
import com.nutech.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/")
	public String home(Model model)
	{
		model.addAttribute("command", new RegisterBean());	
		return "home";
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String register(@ModelAttribute("register") RegisterBean register) throws SQLException{
		  int out=userService.create(register);
		  System.out.println("First==========="+out);
		  return "redirect:/viewuser";
		
	}
	
	 @RequestMapping("/viewuser")    
	    public String viewuser(Model m){    
	        List<RegisterBean> list=userService.getUsers();  
	        System.out.println("==============="+list.size());
	        m.addAttribute("userlist",list);  
	        return "viewuser";    
	    }    
	 
	 @RequestMapping(value="/deleteuser/{id}",method = RequestMethod.GET)    
	    public String delete(@PathVariable int id){    
		 userService.delete(id);    
	        return "redirect:/viewuser";    
	    }     
	 
	 @RequestMapping(value="/edituser/{id}")    
	    public String edit(@PathVariable int id, Model m){    
	        RegisterBean reg=userService.getEmpById(id);    
	        System.out.println("++++++++++++++++++"+reg.getId()+reg.getName());
	        m.addAttribute("command",reg);  
	        return "edituser";    
	    }    
	 
	 @RequestMapping(value="/editsave",method = RequestMethod.POST)    
	    public String editsave(@ModelAttribute("reg") RegisterBean reg){    
	        int out=userService.update(reg);    
	        System.out.println("Second=============="+out);
	        return "redirect:/viewuser";    
}
	
}

