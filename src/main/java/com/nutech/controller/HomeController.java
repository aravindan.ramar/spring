package com.nutech.controller;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nutech.bean.RegisterBean;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
		
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes="application/json")	
	public String register(@RequestBody RegisterBean register) throws SQLException{

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle","JIRA","JIRA");  
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery("insert into user_registration(id,user_name,password,user_email) values ("+register.getId()+","+register.getName()+","+register.getPassword()+","+ register.getEmail()+")");
			/*
			 * JdbcTemplate jdbcTemplate = new JdbcTemplate(); jdbcTemplate.
			 * execute("insert into user_registration(id,user_name,password,user_email) values ("
			 * +register.getId()+","+register.getName()+","+register.getPassword()+","+
			 * register.getEmail()+")");
			 */
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		  
		
		return "Success";
		
	}
	

	
}
