package com.nutech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nutech.bean.RegisterBean;
import com.nutech.repository.UserRepository;
import com.nutech.repositoryimpl.UserRepositoryImpl;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public int create(RegisterBean register) {
		return userRepository.create(register);
	}

	public List<RegisterBean> getUsers() {
		
		return userRepository.getUsers();
	}

	public void delete(int id) {
		
		userRepository.delete(id);
		
	}

	public RegisterBean getEmpById(int id) {
		
		return userRepository.getEmpById(id);
	}

	public int update(RegisterBean reg) {
		// TODO Auto-generated method stub
		return userRepository.update(reg);
	}

}
