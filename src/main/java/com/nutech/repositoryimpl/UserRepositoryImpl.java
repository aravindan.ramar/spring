package com.nutech.repositoryimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.nutech.bean.RegisterBean;
import com.nutech.repository.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository {
	@Autowired
	JdbcTemplate template;
	
	public void setTemplate(JdbcTemplate template) {    
	    this.template = template;    
	}
	
	public int create(RegisterBean register) {
		
			String sql = "insert into user_registration(id,user_name,password,user_email) values (seq_user.nextval,'"+register.getName()+"','"+register.getPassword()+"','"+register.getEmail()+"')";
			return template.update(sql);
			
	}
	
	public List<RegisterBean> getUsers() {
		return template.query("select * from user_registration",new RowMapper<RegisterBean>(){    
	        public RegisterBean mapRow(ResultSet rs, int row) throws SQLException {    
	        	RegisterBean e=new RegisterBean();    
	            e.setId(rs.getInt(1));    
	            e.setName(rs.getString(2));    
	            e.setEmail(rs.getString("user_email"));
	            return e;    
	        }    
	    });    
	}

	public int delete(int id) {
		  String sql="delete from user_registration where id="+id+"";    
		    return template.update(sql); 
		
	}

	public RegisterBean getEmpById(int id) {
		  String sql="select * from user_registration where id=?";  
		  /*System.out.println("-----------------------"+template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<RegisterBean>(RegisterBean.class)).getName());
		    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<RegisterBean>(RegisterBean.class));  */
	
		  return template.queryForObject(sql,new Object[]{id},new RowMapper<RegisterBean>(){    
		        public RegisterBean mapRow(ResultSet rs, int row) throws SQLException {    
		        	RegisterBean e=new RegisterBean();    
		            e.setId(rs.getInt(1));    
		            e.setName(rs.getString(2));    
		            e.setEmail(rs.getString("user_email"));
		            return e;    
		        }    
		    });  
	}

	public int update(RegisterBean reg) {
		
		    String sql="update user_registration set user_name='"+reg.getName()+"', user_email='"+reg.getEmail()+"' where id='"+reg.getId()+"'";    
		    return template.update(sql);    
		
	}

}
